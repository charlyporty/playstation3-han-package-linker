A small c program to help with generating a package_link.xml file for 
transferring files between your (*nix) computer and your HAN enabled PlayStation3
using a network.

Build the program with:

`make`

The executable is called:

`a.out`

Running the program without arguments gives help text:

```
$ ./a.out

USEAGE: ./a.out server_location 1.pkg 2.pkg ...
        i.e. ./a.out 10.0.0.1 game.pkg rap.pkg

```
In case the help text is a little confusing:

The first argument is the location of the `*.pkg` files you wish to transfer to
the PS3. 

For example, on an ArchLinux system using the Apache Server, if my `*.pkg` files
were placed in `/srv/http`, the location would be the IP Address of my computer.
If my `*.pkg` files were placed in `/srv/http/games` then the location would be
`192.168.1.8/games` (assuming my IP Adress is `192.168.1.8`).

After the IP Address, you can pass as many arguments as you like - each argument
should be the path to a `*.pkg` file.


As a final example:
```
$ ./a.out 192.168.1.7/tmp /srv/http/tmp/game.pkg /srv/http/tmp/rif.pkg
```
Here, `192.168.1.7/tmp` is the first argument to the program - the location
of my package files.

The second and third arguments are the paths to the 2 package files I wish to
transfer to my PS3.

The program will generate a file called `package_link.xml` - you should
copy this file to a USB and plug the USB into the PS3, then:

Run HAN Enabler --> Go to Package Manager

And you can install the listed package files via the new option that pops up,
with a sphere icon.


-------------------------------

As a sidenote, you probably don't want to store your `*.pkg` files in the 
/srv/http (or where ever Apache points its webserver for your *nix) directory 
or have to copy them every time you want to move the files to the PS3 - 
the good news is that you don't have to! You can simply create a Symbolic-Link
to the files in the /srv/http directory to where ever the actual files are on
your filesystem! It makes no difference. 
