#
/*_
Copyright (c) <2018>, <Ankush Patel> <ankushjp4@gmail.com>
This Source Code is released under a (3-Clause)BSD-Style Licence.
All rights reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in the
          documentation and/or other materials provided with the distribution.
        * Neither the name of the author nor the names of its contributors
          may be used to endorse or promote products derived from this software
          without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libgen.h>

/* A Macro to swap byte order (i.e. convret little endian to big endian and vice-versa  */
#define bswap_32(x) ((size_t) ((((unsigned char *) &x)[3] << 0) | (((unsigned char *) &x)[2] << 8) | (((unsigned char *) &x)[1] << 16) | (((unsigned char *) &x)[0] << 24)));

/* help text on how to use the program */
#define HELP_TEXT "USEAGE: ./a.out server_location 1.pkg 2.pkg ...\n\ti.e. ./a.out 10.0.0.1 game.pkg rap.pkg"

/* The magic number for a Sony PlayStation .pkg file (NOTE: BIG ENDIAN) */
#define PKG_MAGIC 0x7F504B47

/* The boilerplate xml code that needs to go at the start of the file */
char xml_start_boilerplate[] = \
	"<XMBML version=\"1.0\">\r\n"
		"\t<View id=\"package_link\">\r\n"
			" \t\t<Attributes>\r\n"
				"\t\t\t<Table key=\"pkg_main\">\r\n"
					"\t\t\t\t<Pair key=\"icon\"><String>/dev_flash/vsh/resource/explore/icon/fah-xmb.png</String></Pair>\r\n"
					"\t\t\t\t<Pair key=\"title\"><String>Direct link to package on PC</String></Pair>\r\n"
					"\t\t\t\t<Pair key=\"info\"><String>Download and Install a Package from PC via webserver</String></Pair>\r\n"
					"\t\t\t\t<Pair key=\"ingame\"><String>disable</String></Pair>\r\n"
				"\t\t\t</Table>\r\n"
			"\t\t</Attributes>\r\n"
   			"\t\t<Items>\r\n"
				"\t\t\t<Query class=\"type:x-xmb/folder-pixmap\" key=\"pkg_main\" attr=\"pkg_main\" src=\"#pkg_items\" />\r\n"
			"\t\t</Items>\r\n"
   		"\t</View>\r\n"
		"\t<View id=\"pkg_items\">\r\n"
  			"\t\t<Attributes>\r\n"
;

/* The location the files will be placed in */
char serverLocation[128];


int key = 1;


int processPKG(const char *path, char buffer[1024])
{
	/* Our position within the buffer array whilst writing */
	int bufPos = 0;

	size_t header = 0;
	/* it is 36 bytes in size, + one for NULL-Terminator */
	char contentid[37];
	/*
	 * the final text we will print for the key="" attribute of the
	 * <Table> tags
	 *
	 * The text will contain "link001" where the '001' portion changes depending
	 * on which package we are currently adding in (i.e. it is equal to the value
	 * of the global variable key)
	 */
	char keyText[8] = "link001";

	/* Open the file (read only ofcrouse) */
	FILE *file = fopen(path, "rb");

	printf("Reading file %s\n", path);

	if (file == NULL) {
		return (1); /* Could not open the file */
	}
	
	/* Make sure the file is infact a pkg file */
	fread(&header, sizeof (int), 1, file);

	/* .PKG files are BIG-ENDIAN, yet x86 is LITTLE-ENDIAN
	 * so lets perform a little conversion!
	 */
	header = bswap_32(header);
	if (header != PKG_MAGIC) {
		/* We have an invalid .PKG file! */
		return (2);
	}

	/* Lets get the content id now */
	/* We need to seek to the correct offset in the file first (0x30, 48 dec) */
	fseek(file, 0x30, SEEK_SET);
	fread(contentid, 1, 36, file);
	/* Add the NULL terminator to the end of the content id string */
	contentid[36] = '\0';

	sprintf(keyText, "link%d", key++);

	/* Now start forming the xml data for this package */
	/* write the key of this entry in the download list */
	bufPos = sprintf(buffer, "\t\t\t<Table key=\"%s\">\r\n", keyText);
	/* This is generic and the same for all entries */
	bufPos = bufPos + sprintf(buffer + bufPos, "\t\t\t\t<Pair key=\"info\"><String>net_package_install</String></Pair>\r\n");
	/* The next two are the path to the file (on the server) */
	bufPos = bufPos + sprintf(buffer + bufPos,
		"\t\t\t\t<Pair key=\"pkg_src\"><String>%s/%s</String></Pair>\r\n",
		serverLocation, basename((char *) path)
	);
	bufPos = bufPos + sprintf(buffer + bufPos,
		"\t\t\t\t<Pair key=\"pkg_src_qa\"><String>%s/%s</String></Pair>\r\n",
		serverLocation, basename((char *) path)
	);
	/* The display name of the file */
	bufPos = bufPos + sprintf(buffer + bufPos,
		"\t\t\t\t<Pair key=\"content_name\"><String>%s</String></Pair>\r\n",
		basename((char *) path)
	);
	/* The content id of the pkg file */
	bufPos = bufPos + sprintf(buffer + bufPos,
		"\t\t\t\t<Pair key=\"content_id\"><String>%s</String></Pair>\r\n",
		contentid
	);
	/* The Icon to display on the XMB - generic and the same for every entry */
	bufPos = bufPos + sprintf(buffer + bufPos,
		"\t\t\t\t<Pair key=\"prod_pict_path\"><String>/dev_flash/vsh/resource/explore/icon/fah-xmb.png</String></Pair>\r\n"
	);
	/* Add the file name as the title (text to display on XMB) */
	bufPos = bufPos + sprintf(buffer + bufPos,
		"\t\t\t\t<Pair key=\"title\"><String>%s</String></Pair>\r\n",
		basename((char *) path)
	);


	sprintf(buffer + bufPos, "\t\t\t</Table>\r\n");
	fclose(file);

	return (0);
}

main(int argc, char *argv[])
{
	int i;
	FILE *ofile = fopen("package_link.xml", "wb");

	if (argc < 3) {
		puts(HELP_TEXT);
		return (1);
	}

	/* store the user's provided server path in a global variable */
	strcpy(serverLocation, argv[1]);

	/* Get the boilerplate markup into the file */
	fputs(xml_start_boilerplate, ofile);

	for (i = 2; i < argc; ++i) {
		char buff[1024];
		int ret = processPKG(argv[i], buff);

		if (ret == 1) {
			puts("File failed to open, does the path exist?");
		}
		if (ret == 2) {
			puts("Not a valid PKG file! File will be Skiped!");
		}

		/* There are no errors, lets write this to the (end of the) file */
		fputs(buff, ofile);
	}

	/* Now add the ending code (which is not all boilerplate */
	fputs("\t\t</Attributes>\r\n", ofile);
	fputs("\t\t<Items>\r\n", ofile);
	for (i = 1; i < key; ++i) {
		char buff[512];
		sprintf(buff, "\t\t\t<Item class=\"type:x-xmb/xmlnpsignup\" key=\"link%d\" attr=\"link%d\" />\r\n", i, i);
		fputs(buff, ofile);
	}
	fputs("\t\t</Items>\r\n", ofile);
	fputs("\t</View>\r\n", ofile);
	fputs("</XMBML>\r\n", ofile);

	fclose(ofile);

	return (0);
}


